package com.siafuinc.eposta.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.siafuinc.eposta.R;

/**
 * Created by 001557 on 10/21/2016.
 */
public  class ReceivedItemsViewHolder extends RecyclerView.ViewHolder {

        public TextView message;
        public TextView TextViewTypeAlertBillSummon;
        public TextView textViewFileName;
        public TextView textViewPdf;
        public TextView textViewAddedTime;
        public ImageView imageViewSenderProfile;
        public TextView textViewDisplyName;


        public Button btnPayShare;
    public Button btnPay;
        public TextView textViewAmount;

        public TextView textViewCurrency;
    public LinearLayout linearLayoutBill;




        public ReceivedItemsViewHolder(View itemView) {
            super(itemView);

            TextViewTypeAlertBillSummon=(TextView) itemView.findViewById(R.id.textViewTypeAlertBillSummon);
            textViewFileName=(TextView) itemView.findViewById(R.id.textViewFileName);
            textViewPdf=(TextView) itemView.findViewById(R.id.textViewPdf);

            btnPayShare=(Button) itemView.findViewById(R.id.btnPayShare);
            btnPay=(Button) itemView.findViewById(R.id.btnPay);

            textViewAmount=(TextView) itemView.findViewById(R.id.textViewAmount);

            textViewAddedTime=(TextView) itemView.findViewById(R.id.textViewAddedTime);
            imageViewSenderProfile=(ImageView) itemView.findViewById(R.id.imageViewSenderProfile);
            textViewDisplyName=(TextView) itemView.findViewById(R.id.textViewDisplyName);
            message = (TextView) itemView.findViewById(R.id.textview1);

            textViewCurrency=(TextView) itemView.findViewById(R.id.textViewCurrency);
            linearLayoutBill=(LinearLayout) itemView.findViewById(R.id.linear_layout_bill);
        }

}
