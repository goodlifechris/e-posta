package com.siafuinc.eposta.adapter;

import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.siafuinc.eposta.R;
import com.siafuinc.eposta.activities.MainEboxActivity;
import com.siafuinc.eposta.model.ItemsReceived;
import com.siafuinc.eposta.services.SystemAlertWindowService;
import com.siafuinc.eposta.utils.PDFTools;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * Created by 001557 on 10/21/2016.
 */


public class ReceivedItemsAdapter extends RecyclerView.Adapter<ReceivedItemsViewHolder> {

    private Context mContext;
    private DatabaseReference mDatabaseReference;
    private ChildEventListener mChildEventListener;
    static SimpleDateFormat sdf;

    DownloadManager  downloadManager;

    public String filename;
    public static String TAG="Adapter";

    // Progress Dialog
    private ProgressDialog pDialog;
    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;
    private int count = 0;
    private String business_number = "123456";
    private String account_number = "654321";
    private String amount_to_pay = "310";

    private List<String> mCommentIds = new ArrayList<>();
    private List<ItemsReceived> itemsReceivedList = new ArrayList<>();


    FirebaseStorage storage;

    public ReceivedItemsAdapter(final Context context, DatabaseReference ref) {
        mContext = context;
        mDatabaseReference = ref;
        pDialog = new ProgressDialog(mContext);




        // Create child event listener
        // [START child_event_listener_recycler]
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d("TAG", "onChildAdded:" + dataSnapshot.getKey());

                // A new comment has been added, add it to the displayed list
                ItemsReceived itemsReceived = dataSnapshot.getValue(ItemsReceived.class);
                Toast.makeText(context,"A new message has arrived ",Toast.LENGTH_LONG);

                // [START_EXCLUDE]
                // Update RecyclerView
                mCommentIds.add(dataSnapshot.getKey());
                itemsReceivedList.add(itemsReceived);
                notifyItemInserted(itemsReceivedList.size() - 1);
                // [END_EXCLUDE]
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG, "onChildChanged:" + dataSnapshot.getKey());

                // A comment has changed, use the key to determine if we are displaying this
                // comment and if so displayed the changed comment.
                ItemsReceived newItemsReceived = dataSnapshot.getValue(ItemsReceived.class);
                String commentKey = dataSnapshot.getKey();

                // [START_EXCLUDE]
                int commentIndex = mCommentIds.indexOf(commentKey);
                if (commentIndex > -1) {
                    // Replace with the new data
                    itemsReceivedList.set(commentIndex, newItemsReceived);

                    // Update the RecyclerView
                    notifyItemChanged(commentIndex);
                } else {
                    Log.w(TAG, "onChildChanged:unknown_child:" + commentKey);
                }
                // [END_EXCLUDE]
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onChildRemoved:" + dataSnapshot.getKey());

                // A comment has changed, use the key to determine if we are displaying this
                // comment and if so remove it.
                String commentKey = dataSnapshot.getKey();

                // [START_EXCLUDE]
                int commentIndex = mCommentIds.indexOf(commentKey);
                if (commentIndex > -1) {
                    // Remove data from the list
                    mCommentIds.remove(commentIndex);
                    itemsReceivedList.remove(commentIndex);

                    // Update the RecyclerView
                    notifyItemRemoved(commentIndex);
                } else {
                    Log.w(TAG, "onChildRemoved:unknown_child:" + commentKey);
                }
                // [END_EXCLUDE]
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG, "onChildMoved:" + dataSnapshot.getKey());

                // A comment has changed position, use the key to determine if we are
                // displaying this comment and if so move it.
                ItemsReceived movedItemsReceived = dataSnapshot.getValue(ItemsReceived.class);
                String commentKey = dataSnapshot.getKey();

                // ...
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "postComments:onCancelled", databaseError.toException());
                Toast.makeText(mContext, "Failed to load comments.",
                        Toast.LENGTH_SHORT).show();
            }
        };
        ref.addChildEventListener(childEventListener);
        // [END child_event_listener_recycler]


        // Store reference to listener so it can be removed on app stop
        mChildEventListener = childEventListener;
    }



    @Override
    public ReceivedItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_recieved_layout, parent, false);
        return new ReceivedItemsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ReceivedItemsViewHolder holder, int position) {
        final ItemsReceived itemsReceived = itemsReceivedList.get(position);
        holder.message.setText(itemsReceived.getMessage());
        holder.TextViewTypeAlertBillSummon.setText(itemsReceived.getType());



        holder.textViewFileName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                }

        });

        holder.textViewFileName.setVisibility(View.GONE);
        holder.textViewPdf.setVisibility(View.GONE);
        holder.linearLayoutBill.setVisibility(View.GONE);

        if (itemsReceived.getAttachments()!=null){
            holder.textViewFileName.setVisibility(View.VISIBLE);
            holder.textViewFileName.setText(itemsReceived.getAttachments().get(0).getName());

            holder.textViewPdf.setVisibility(View.VISIBLE);
            filename=itemsReceived.getAttachments().get(0).getName();
            holder.textViewPdf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new DownloadFileFromURL().execute(itemsReceived.getAttachments().get(0).getDownloadURLs().get(0));
                }
            });
            holder.textViewFileName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new DownloadFileFromURL().execute(itemsReceived.getAttachments().get(0).getDownloadURLs().get(0));
                }
            });
        }
        if (itemsReceived.getType().equals("Bill")){
            holder.linearLayoutBill.setVisibility(View.VISIBLE);
            holder.TextViewTypeAlertBillSummon.setTextColor(mContext.getResources().getColor(R.color.colorBill));
            holder.TextViewTypeAlertBillSummon.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.circle_bill),null,null,null);

            holder.btnPayShare.setVisibility(View.GONE);

            holder.textViewAmount.setText(itemsReceived.getData().getAmount());
            holder.textViewCurrency.setText("Ksh");
            holder.textViewCurrency.setVisibility(View.VISIBLE);
            holder.textViewAmount.setVisibility(View.VISIBLE);
            holder.btnPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showSTK(itemsReceived.getData().getPayBill(),itemsReceived.getData().getAccount(),itemsReceived.getData().getAmount());

                }
            });
        }
        else if (itemsReceived.getType().equals("Alert")){
            holder.btnPayShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, itemsReceived.getMessage()); // Simple text and URL to share
                    if (itemsReceived.getSender()!=null)sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, itemsReceived.getSender().getDisplayName());
                    sendIntent.setType("text/plain");
                    mContext.startActivity(sendIntent);
                }
            });
            holder.btnPayShare.setVisibility(View.VISIBLE);
            holder.btnPayShare.setText("SHARE ALERT");


            holder.TextViewTypeAlertBillSummon.setTextColor(mContext.getResources().getColor(R.color.colorRed));
            holder.TextViewTypeAlertBillSummon.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.circle_alert),null,null,null);

        }
        else {
            holder.TextViewTypeAlertBillSummon.setTextColor(mContext.getResources().getColor(R.color.colorSummon));
            holder.TextViewTypeAlertBillSummon.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.circle_summon),null,null,null);

        }
        sdf = new SimpleDateFormat("MMMM d, yyyy 'at' h:mm a");

        holder.textViewAddedTime.setText(sdf.format(itemsReceived.getAdded()));

        Picasso.with(mContext).load(itemsReceived.getSenderProfile()).into(holder.imageViewSenderProfile);

        if (itemsReceived.getSender()!=null)
            holder.textViewDisplyName.setText(itemsReceived.getSender().getDisplayName());


    }

    @Override
    public int getItemCount() {
        return itemsReceivedList.size();
    }


    public void cleanupListener() {
        if (mChildEventListener != null) {
            mDatabaseReference.removeEventListener(mChildEventListener);
        }
    }
    public void showSTK(String business_number, String account_number, String amount_to_pay){

        try{
            Intent stk = mContext.getPackageManager().getLaunchIntentForPackage("com.android.stk");
            if (stk != null)
                mContext.startActivity(stk);

            Intent intent = new Intent(mContext, SystemAlertWindowService.class);
            intent.putExtra(mContext.getString(R.string.business_number), business_number);
            intent.putExtra(mContext.getString(R.string.account_number), account_number);
            intent.putExtra(mContext.getString(R.string.amount_to_pay), amount_to_pay);
            mContext.startService(intent);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Background Async Task to download file
     * */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            pDialog = new ProgressDialog(mContext);
            pDialog.setMessage("Downloading "+filename+".Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setMax(100);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                OutputStream output = new FileOutputStream("/sdcard/"+filename);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress(""+(int)((total*100)/lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded

            pDialog.dismiss();

            // Displaying downloaded image into image view
            // Reading image path from sdcard
            String imagePath = Environment.getExternalStorageDirectory().toString();
            Log.e(TAG,"Thsis is the only"+file_url+"Whats"+imagePath);
            // setting downloaded into image view
//            .setImageDrawable(Drawable.createFromPath(imagePath));
            File file = new File(Environment.getExternalStorageDirectory(),
                    filename);
            Uri path = Uri.fromFile(file);

            Intent pdfOpenintent = new Intent(Intent.ACTION_VIEW);
            pdfOpenintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


            pdfOpenintent.setDataAndType(path, "*/*");
            try {
                mContext.startActivity(pdfOpenintent);
            }
            catch (ActivityNotFoundException e) {

            }
        }

    }
    public void firebaseshit(){
        storage = FirebaseStorage.getInstance();

//        // Create a storage reference from our app
//        StorageReference storageRef = storage.getReferenceFromUrl("gs://eposta-bf12b.appspot.com");
//        StorageReference specificFileToDownload = storageRef.child(itemsReceived.getAttachments().get(0).getFullPath());
//        Log.e(TAG,itemsReceived.getAttachments().get(0).getFullPath());
//
//        File localFile = null;
//        try {
//            localFile = File.createTempFile(itemsReceived.getAttachments().get(0).getName(), "pdf");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        specificFileToDownload.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
//            @Override
//            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
//                // Local temp file has been created
//
//                Log.e(TAG,"local files has been downloaded successfully"+taskSnapshot.getBytesTransferred()+taskSnapshot.getStorage());
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception exception) {
//                // Handle any errors
//                Log.e(TAG,"local files has not been downloaded successfully");
//            }
//        });
    }

}
