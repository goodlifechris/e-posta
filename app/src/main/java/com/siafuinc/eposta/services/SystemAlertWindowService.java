package com.siafuinc.eposta.services;

import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.siafuinc.eposta.R;

public class SystemAlertWindowService extends Service {
    View systemAlertView;
    public String business_number = "Business No.: ";
    public String account_number = "Account No.: ";
    public String amount_to_pay = "Amount Ksh: ";

    ClipboardManager clipboard;


    public SystemAlertWindowService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException(getString(R.string.not_yet_implemented));
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart(Intent intent, int startId) {
        try {

            clipboard= (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

            business_number = intent.getStringExtra(getString(R.string.business_number));
            account_number = intent.getStringExtra(getString(R.string.account_number));
            amount_to_pay = intent.getStringExtra(getString(R.string.amount_to_pay));

            WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                    400, ViewGroup.LayoutParams.WRAP_CONTENT, 200, 0,
                    WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                            | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                            | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSLUCENT);

            final WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
            final LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            systemAlertView = inflater.inflate(R.layout.system_alert_window_layout, null);

            ImageView img_close = (ImageView) systemAlertView.findViewById(R.id.img_close);

            TextView tv_business_number = (TextView) systemAlertView.findViewById(R.id.tv_business_number);
            tv_business_number.setText(business_number);

            final TextView tv_account_number = (TextView) systemAlertView.findViewById(R.id.tv_account_number);
            tv_account_number.setText(account_number);

            TextView tv_amount_to_pay = (TextView) systemAlertView.findViewById(R.id.tv_amount_to_pay);
            tv_amount_to_pay.setText(amount_to_pay);

            img_close.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    systemAlertView.setVisibility(View.GONE);
                    return true;
                }
            });
            tv_account_number.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    ClipData clip = ClipData.newPlainText("acount_number","123455");
                    clipboard.setPrimaryClip(clip);
                    return false;

                }
            });
            tv_account_number.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ClipData clip = ClipData.newPlainText("acount_number",account_number);
                    clipboard.setPrimaryClip(clip);



                }
            });
            tv_business_number.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ClipData clip = ClipData.newPlainText("acount_number", business_number);
                    clipboard.setPrimaryClip(clip);
                    tv_account_number.setTextColor(getResources().getColor(R.color.colorRed));



                }
            });
            tv_amount_to_pay.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("acount_number", amount_to_pay);
                    clipboard.setPrimaryClip(clip);
                }
            });

            params.gravity = Gravity.TOP;

            wm.addView(systemAlertView, params);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        if(systemAlertView!=null) {
            systemAlertView.setVisibility(View.GONE);
        }
    }
}
