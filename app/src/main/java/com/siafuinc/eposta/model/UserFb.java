package com.siafuinc.eposta.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by 001557 on 10/16/2016.
 */

@IgnoreExtraProperties
public class UserFb {

    public String POB;
    public String eBoxIndex;
    public String email;
    public String name;
    public String postalCode;

    public UserFb() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public UserFb(String POB, String eBoxIndex, String email, String name, String postalCode) {
        this.POB = POB;
        this.eBoxIndex = eBoxIndex;
        this.email = email;
        this.name = name;
        this.postalCode = postalCode;
    }


}