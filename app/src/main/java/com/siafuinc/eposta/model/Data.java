package com.siafuinc.eposta.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by 001557 on 10/17/2016.
 */
@IgnoreExtraProperties

public class Data {
    private String account,amount;
    private String payBill;

    public Data() {
    }

    public Data(String account, String amount, String paid, String payBill) {
        this.account = account;
        this.amount = amount;
        this.payBill = payBill;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }



    public String getPayBill() {
        return payBill;
    }

    public void setPayBill(String payBill) {
        this.payBill = payBill;
    }
}
