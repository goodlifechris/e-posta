package com.siafuinc.eposta.model;

/**
 * Created by 001557 on 10/17/2016.
 */
public class AttachmentsRecord {
   private String bucket;
    private String contentEncoding;
    private String contentType;

    public AttachmentsRecord() {
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getContentEncoding() {
        return contentEncoding;
    }

    public void setContentEncoding(String contentEncoding) {
        this.contentEncoding = contentEncoding;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
