package com.siafuinc.eposta.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;

/**
 * Created by 001557 on 10/17/2016.
 */
@IgnoreExtraProperties

public class Attachments {

    private String bucket;
    private String contentEncoding;
    private String contentType;
    private String fullPath;
    private List<String> downloadURLs;
    private String name;
    private long size;

    public Attachments() {
    }

    public Attachments(String bucket, String contentEncoding, String contentType, String fullPath, List<String> downloadURLs, String name, long size) {
        this.bucket = bucket;
        this.contentEncoding = contentEncoding;
        this.contentType = contentType;
        this.fullPath = fullPath;
        this.downloadURLs = downloadURLs;
        this.name = name;
        this.size = size;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getContentEncoding() {
        return contentEncoding;
    }

    public void setContentEncoding(String contentEncoding) {
        this.contentEncoding = contentEncoding;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public List<String> getDownloadURLs() {
        return downloadURLs;
    }

    public void setDownloadURLs(List<String> downloadURLs) {
        this.downloadURLs = downloadURLs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}
