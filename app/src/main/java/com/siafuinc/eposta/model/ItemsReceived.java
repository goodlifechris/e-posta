package com.siafuinc.eposta.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 001557 on 10/16/2016.
 */
@IgnoreExtraProperties
public class ItemsReceived {

    private String to;

    private long added;

    private String error;

    private String subject;

    public Data data;

    private List<Attachments> attachments;

    private String from;

    private String type;

    private String amount;

    private String message;

    Sender sender;


//    private String sending;

//    private String sent;

    private String payBill;

    private String senderUID;

    List<String> address = new ArrayList<>();

//    private String correctAddress;

    private String account;

//    private String read;

    private String senderProfile;

    public ItemsReceived() {
    }


    public ItemsReceived(String to, long added, String error, String subject, Data data, List<Attachments> attachments, String from, String type, String amount, String message, Sender sender, String payBill, String senderUID, List<String> address, String account, String senderProfile) {
        this.to = to;
        this.added = added;
        this.error = error;
        this.subject = subject;
        this.data = data;
        this.attachments = attachments;
        this.from = from;
        this.type = type;
        this.amount = amount;
        this.message = message;
        this.sender = sender;
        this.payBill = payBill;
        this.senderUID = senderUID;
        this.address = address;
        this.account = account;
        this.senderProfile = senderProfile;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public long getAdded() {
        return added;
    }

    public void setAdded(long added) {
        this.added = added;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public List<Attachments> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachments> attachments) {
        this.attachments = attachments;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public String getPayBill() {
        return payBill;
    }

    public void setPayBill(String payBill) {
        this.payBill = payBill;
    }

    public String getSenderUID() {
        return senderUID;
    }

    public void setSenderUID(String senderUID) {
        this.senderUID = senderUID;
    }

    public List<String> getAddress() {
        return address;
    }

    public void setAddress(List<String> address) {
        this.address = address;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getSenderProfile() {
        return senderProfile;
    }

    public void setSenderProfile(String senderProfile) {
        this.senderProfile = senderProfile;
    }
}