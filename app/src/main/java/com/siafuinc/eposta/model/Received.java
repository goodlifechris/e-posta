package com.siafuinc.eposta.model;

/**
 * Created by 001557 on 10/16/2016.
 */

public class Received {

    public ItemsReceived itemsReceived;

    public Received() {
    }

    public ItemsReceived getItemsReceived() {
        return itemsReceived;
    }

    public void setItemsReceived(ItemsReceived itemsReceived) {
        this.itemsReceived = itemsReceived;
    }
}
