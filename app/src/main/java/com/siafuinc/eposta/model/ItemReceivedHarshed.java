package com.siafuinc.eposta.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;

/**
 * Created by 001557 on 10/17/2016.
 */
@IgnoreExtraProperties
public class ItemReceivedHarshed {
    public HashMap<String, ItemsReceived> itemsReceivedHashMap;

    public ItemReceivedHarshed() {
    }

    public HashMap<String, ItemsReceived> getItemsReceivedHashMap() {
        return itemsReceivedHashMap;
    }

    public void setItemsReceivedHashMap(HashMap<String, ItemsReceived> itemsReceivedHashMap) {
        this.itemsReceivedHashMap = itemsReceivedHashMap;
    }
}
