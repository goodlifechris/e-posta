package com.siafuinc.eposta.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.siafuinc.eposta.LoginActivity;
import com.siafuinc.eposta.R;
import com.siafuinc.eposta.fragments.MainMyEBoxAndPhysicalAccount;
import com.siafuinc.eposta.fragments.MyBox;
import com.siafuinc.eposta.fragments.MyProfileFragment;
import com.siafuinc.eposta.fragments.MySubscriptionFragment;
import com.siafuinc.eposta.fragments.PostaCloudFragment;
import com.siafuinc.eposta.model.ItemsReceived;
import com.siafuinc.eposta.services.SystemAlertWindowService;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainEboxActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private static final String TAG = "PostDetailActivity";

    public static final String EXTRA_POST_KEY = "post_key";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;



    boolean doubleBackToExitPressedOnce = false;



    private int selectedNavMenu;
    SharedPreferences mPrefs;

    private RecyclerView mCommentsRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      mPrefs= PreferenceManager.getDefaultSharedPreferences(this);
        if (mPrefs.getAll().isEmpty()){
            startActivity(new Intent(this,LoginActivity.class));
        }else{

            setContentView(R.layout.activity_main);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            ButterKnife.bind(this);

            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);


            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            String name = mPrefs.getString("name",null);
            String email=mPrefs.getString("email",null);
            String photoUrl=mPrefs.getString("photoUrl",null);

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            View navHeaderView = navigationView.inflateHeaderView(R.layout.nav_header_main);

            TextView textViewName = (TextView)navHeaderView.findViewById(R.id.textViewUserName);
            TextView textViewEmail = (TextView)navHeaderView.findViewById(R.id.textViewEmail);
            CircleImageView imageViewPhoto=(CircleImageView) navHeaderView.findViewById(R.id.imageView);

            Log.e(TAG,email);
            textViewEmail.setText(email);
            textViewName.setText(name);
            Picasso.with(this).load(photoUrl).placeholder(R.drawable.logo_56dp).fit().centerCrop().into(imageViewPhoto);


            navigationView.setNavigationItemSelectedListener(this);

            if(savedInstanceState==null){
                MenuItem menuItem = navigationView.getMenu().findItem(R.id.nav_my_box);
                onNavigationItemSelected(menuItem);
                menuItem.setChecked(true);
            }
            else {
                if(getSupportActionBar()!=null) getSupportActionBar().setTitle(getActiveFragmentTitle());
            }

            getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    if(getSupportActionBar()!=null) getSupportActionBar().setTitle(getActiveFragmentTitle());

                    checkMenu();

                    //TODO IF WORKING WITH A SINGLE TAB LIKE A PRO
//                if (getActiveFragmentTitle().equals(getString(R.string.title_profile))||getActiveFragmentTitle().equals(getString(R.string.title_profile_church))||getActiveFragmentTitle().equals(getString(R.string.title_my_groups))) {
//                    tabLayout.setVisibility(View.VISIBLE);
//                }
//                else {
//                    tabLayout.setVisibility(View.GONE);
//                }
                }
            });

        }


    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

         if (drawer!=null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else if(isMyServiceRunning(SystemAlertWindowService.class)){
            Intent intent = new Intent(this, SystemAlertWindowService.class);
            stopService(intent);
        }

            else if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }



        else {
            int fragments = getSupportFragmentManager().getBackStackEntryCount();
            if (fragments == 1) {
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce=false;
                    }
                }, 2000);
                finish();
            } else {
                if (getFragmentManager().getBackStackEntryCount() > 1) {
                    getFragmentManager().popBackStack();
                } else {
                    super.onBackPressed();
                }
            }

        }
    }

    public void checkMenu() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            return;
        }
        String tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();

        if(tag.equals(getString(R.string.title_my_box))) {selectedNavMenu = R.id.nav_my_box;}
        else if(tag.equals(getString(R.string.title_my_subscription_services))) {selectedNavMenu = R.id.nav_subscribe;}
//        else if(tag.equals(getString(R.string.title_posta_voice))) {selectedNavMenu = R.id.nav_posta_voice;}
        else if(tag.equals(getString(R.string.title_my_profile))) {selectedNavMenu = R.id.nav_my_profile;}

        if(selectedNavMenu!=0) navigationView.getMenu().findItem(selectedNavMenu).setChecked(true);

    }

    public String getActiveFragmentTitle() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            return getString(R.string.app_name);
        }
        return getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_my_box:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_main, new MainMyEBoxAndPhysicalAccount(), getString(R.string.title_my_box))
                        .addToBackStack(getString(R.string.title_my_box))
                        .commit();
                break;


            case R.id.nav_subscribe:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_main, new MySubscriptionFragment(), getString(R.string.title_my_subscription_services))
                        .addToBackStack(getString(R.string.title_my_subscription_services))
                        .commit();
                break;
            case R.id.nav_my_profile:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_main, new MyProfileFragment(), getString(R.string.title_my_profile))
                        .addToBackStack(getString(R.string.title_my_profile))
                        .commit();
                break;
            case R.id.nav_logout:

                FirebaseAuth.getInstance().signOut();
                SharedPreferences.Editor editor = mPrefs.edit();
                editor.clear().commit();

                Intent i = new Intent(this, LoginActivity.class);

                startActivity(i);
                finish();

                break;

        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
