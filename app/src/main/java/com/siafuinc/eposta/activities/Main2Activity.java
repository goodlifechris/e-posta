package com.siafuinc.eposta.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.siafuinc.eposta.R;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.siafuinc.eposta.model.ItemReceivedHarshed;
import com.siafuinc.eposta.model.ItemsReceived;
import com.siafuinc.eposta.model.UserEboxFb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = Main2Activity.class.getSimpleName();
    //Getting reference to Firebase Database
    // [START declare_database_ref]
    private DatabaseReference mFirebaseDatabaseReference;

    // [END declare_database_ref]
    private List<ItemsReceived> allItemsReceived;
    RecyclerView mRecyclerView;

//    private FirebaseRecyclerAdapter mFirebaseAdapter;

    private FirebaseRecyclerAdapter<ItemReceivedHarshed, ItemsReceivedHolder> mRecyclerViewAdapter;
    private LinearLayoutManager mManager;


    private ValueEventListener mPostListener;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mRecyclerView=(RecyclerView) findViewById(R.id.recyclerView);
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference("eboxes").child("277-30500").child("0").child("received");
        mManager = new LinearLayoutManager(this);
        mManager.setReverseLayout(false);

        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(mManager);
// /        mFirebaseDatabaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot snapshot) {
//                for (DataSnapshot msgSnapshot: snapshot.getChildren()) {
//                    Log.e(TAG,"Please help "+msgSnapshot.getChildren().toString());
//                }
//            }
//            @Override
//            public void onCancelled(DatabaseError firebaseError) {
//                Log.e("Chat", "The read failed: " + firebaseError.getDetails());
//            }
//        });
//
//// Initialize Database
//        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference()
//                .child("posts").child(mPostKey);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("Filter by type");
        spinnerArray.add("type 2");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.custom_spinner_textview, spinnerArray);

        adapter.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        Spinner sItems = (Spinner) findViewById(R.id.spinner_filty_by_type);
        sItems.setAdapter(adapter);



    }
//    private void setUpFirebaseAdapter(  DatabaseReference mFirebaseDatabaseReference1) {
//
//        mFirebaseAdapter = new FirebaseRecyclerAdapter<ItemsReceived, ItemsRecievedListAdapter.ItemsReceivedAdaperViewHolder>
//                (ItemsReceived.class, R.layout.item_recieved_layout, ItemsRecievedListAdapter.ItemsReceivedAdaperViewHolder.class,
//                        mFirebaseDatabaseReference1){
//
//            @Override
//            protected void populateViewHolder(ItemsRecievedListAdapter.ItemsReceivedAdaperViewHolder viewHolder, ItemsReceived model, int position) {
//                viewHolder.bindRestaurant(model);
//
//            }
//        };
//        mRecyclerView.setHasFixedSize(true);
//        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
//        mRecyclerView.setAdapter(mFirebaseAdapter);
//    }

//    private void setUpFirebaseAdapter() {
//        mFirebaseAdapter = new FirebaseRecyclerAdapter<ItemsReceived, EboxViewHolder>
//                (ItemsReceived.class, R.layout.item_recieved_layout, EboxViewHolder.class,
//                        mFirebaseDatabaseReference) {
//
//            @Override
//            protected void populateViewHolder(EboxViewHolder viewHolder,
//                                              ItemsReceived model, int position) {
//                viewHolder.bindRestaurant(model);
//            }
//        };
//        mRecyclerView.setHasFixedSize(true);
//        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
//        mRecyclerView.setAdapter(mFirebaseAdapter);
//    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mRecyclerViewAdapter != null) {
            mRecyclerViewAdapter.cleanup();
        }    }
    @Override
    protected void onStart() {
        super.onStart();

//        mFirebaseDatabaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                final Map<String, ItemsReceived> itemsReceivedMap = new LinkedHashMap<String, ItemsReceived>();
//                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
//
//                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
//                        HashMap<String, ItemsReceived> messageMap = (HashMap<String, ItemsReceived>) postSnapshot.getValue();
//                        Collection<ItemsReceived> messageItems = messageMap.values();
//                        List<ItemsReceived> messageItemList = new ArrayList<ItemsReceived>();
//                        messageItemList.addAll(messageItems);
//                        Log.e(TAG,"The value are "+messageItemList.toString());
//                    }
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

        // Add value event listener to the post
        // [START post_value_event_listener]
//        ValueEventListener postListener = new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                // Get Post object and use the values to update the UI
//                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
//                    Log.e("Count " ,""+dataSnapshot.getChildrenCount());
//                    Map<String, Object> objectMap = (HashMap<String, Object>)
//                            dataSnapshot.getValue();
//                    ArrayList<ItemsReceived> itemsReceivedList = new ArrayList<>();
//
//                    for (Object obj : objectMap.values()) {
//                        if (obj instanceof Map) {
//                            Map<String, Object> mapObj = (Map<String, Object>) obj;
//                            ItemsReceived itemsReceived = new ItemsReceived();
//                            itemsReceived.setAccount((String) mapObj.get("account"));
//                            itemsReceived.setAdded((long) mapObj.get("added"));
////                            itemsReceived.setData((List<String>) mapObj.get("data"));
//
//
//
//                            itemsReceivedList.add(itemsReceived);
//
//                            Log.e(TAG ,"Data is" +itemsReceived.getAdded());
//
//                        }
//                    }
//                    setUpFirebaseAdapter(mFirebaseDatabaseReference);
//
////                    itemsRecievedListAdapter = new ItemsRecievedListAdapter(MainActivity.this, itemsReceivedList);
////                        mRecyclerView.setAdapter(itemsRecievedListAdapter);
////                        RecyclerView.LayoutManager layoutManager =
////                                new LinearLayoutManager(MainActivity.this);
////                        mRecyclerView.setLayoutManager(layoutManager);
////                        mRecyclerView.setHasFixedSize(true);
//////
////                    for (DataSnapshot msgSnapshot: dataSnapshot.getChildren()) {
//////                        ItemsReceived itemsReceived = msgSnapshot.getValue(ItemsReceived.class);
////
////                        HashMap<String,ItemsReceived> stringItemsReceivedHashMap = (HashMap<String, ItemsReceived>) msgSnapshot.getValue();
////                        Collection<ItemsReceived> itemsReceivedCollection = stringItemsReceivedHashMap.values() ;
////                        List<ItemsReceived> messageItemList = new ArrayList<ItemsReceived>();
////                        messageItemList.addAll(itemsReceivedCollection);
//////                        Log.e("Address " ,""+messageItemList.get(itemsReceivedCollection.size()).getAdded());
////                        itemsRecievedListAdapter = new ItemsRecievedListAdapter(MainActivity.this, messageItemList);
////                        mRecyclerView.setAdapter(itemsRecievedListAdapter);
////                        RecyclerView.LayoutManager layoutManager =
////                                new LinearLayoutManager(MainActivity.this);
////                        mRecyclerView.setLayoutManager(layoutManager);
////                        mRecyclerView.setHasFixedSize(true);
////
////                    }
//
////
//                }
//                // [END_EXCLUDE]
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                // Getting Post failed, log a message
//                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
//                // [START_EXCLUDE]
//                Toast.makeText(MainActivity.this, "Failed to load post.",
//                        Toast.LENGTH_SHORT).show();
//                // [END_EXCLUDE]
//            }
//        };
//        mFirebaseDatabaseReference.addValueEventListener(postListener);
//        // [END post_value_event_listener]
//
//        // Keep copy of post listener so we can remove it when app stops
//        mPostListener = postListener;
        attachRecyclerViewAdapter();
//        // Listen for comments
//        mAdapter = new CommentAdapter(this, mCommentsReference);
//        mCommentsRecycler.setAdapter(mAdapter);
    }

    //    TODO UNCOMMENT FOR LISTENERS TO WORK
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void attachRecyclerViewAdapter() {
//        Query lastFifty = mFirebaseDatabaseReference.limitToLast(50);
        Query queryRef = mFirebaseDatabaseReference.orderByKey();
        queryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                for (DataSnapshot messageSnapshot: dataSnapshot.getChildren()) {
//                 ItemReceivedHarshed itemReceivedHarshed=messageSnapshot.getValue(ItemReceivedHarshed.class);
//                    HashMap<String,ItemsReceived> stringItemsReceivedHashMap = (HashMap<String, ItemsReceived>) messageSnapshot.getValue();
//                        Collection<ItemsReceived> itemsReceivedCollection = stringItemsReceivedHashMap.values() ;
//                        List<ItemsReceived> messageItemList = new ArrayList<ItemsReceived>();
//                        messageItemList.addAll(itemsReceivedCollection);
//                }
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    Log.e("Count ", "" + dataSnapshot.getChildrenCount());
                    Map<String, Object> objectMap = (HashMap<String, Object>)
                            dataSnapshot.getValue();
                    ArrayList<ItemsReceived> itemsReceivedList = new ArrayList<>();

                    for (Object obj : objectMap.values()) {
                        if (obj instanceof Map) {
                            Map<String, Object> mapObj = (Map<String, Object>) obj;
                            ItemsReceived itemsReceived = new ItemsReceived();
                            itemsReceived.setAccount((String) mapObj.get("account"));
                            itemsReceived.setAdded((long) mapObj.get("added"));
//                            itemsReceived.setData((List<String>) mapObj.get("data"));


                            itemsReceivedList.add(itemsReceived);

                            Log.e(TAG, "Data is" + itemsReceived.getAdded());


                        }
                    }

//                    itemsRecievedListAdapter = new ItemsRecievedListAdapter(Main2Activity.this, itemsReceivedList);
//                    mRecyclerView.setAdapter(itemsRecievedListAdapter);
//
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
//        mRecyclerViewAdapter  = new FirebaseRecyclerAdapter<ItemReceivedHarshed, ItemsReceivedHolder>(
//                ItemReceivedHarshed.class, R.layout.item_recieved_layout, ItemsReceivedHolder.class, mFirebaseDatabaseReference) {
//
//            @Override
//            public void populateViewHolder(ItemsReceivedHolder chatView, ItemReceivedHarshed itemReceivedHarshed, int position) {
//                List<ItemsReceived> al = new ArrayList<ItemsReceived>(hashMapVar.values());
//
//                chatView.setName(String.valueOf(itemReceivedHarshed.getItemsReceivedHashMap().get(position).getAdded()));
//
//
//            }
//        };

        // Scroll to bottom on new messages
//        mRecyclerViewAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
//            @Override
//            public void onItemRangeInserted(int positionStart, int itemCount) {
//                mManager.smoothScrollToPosition(mRecyclerView, null, mRecyclerViewAdapter.getItemCount());
//            }
//        });

    }

    public static class ItemsReceivedHolder extends RecyclerView.ViewHolder {
        View mView;

        public ItemsReceivedHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setName(String name) {
            TextView field = (TextView) mView.findViewById(R.id.textview1);
            field.setText(name);
        }


    }
}
