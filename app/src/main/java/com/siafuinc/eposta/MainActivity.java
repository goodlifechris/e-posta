package com.siafuinc.eposta;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import com.siafuinc.eposta.model.Data;
import com.siafuinc.eposta.model.ItemsReceived;
import com.siafuinc.eposta.model.UserEboxFb;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static java.security.AccessController.getContext;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    //Getting reference to Firebase Database
    // [START declare_database_ref]

    // [END declare_database_ref]
    RecyclerView mRecyclerView;


//    private  mFirebaseAdapter;

    private LinearLayoutManager mManager;
    static SimpleDateFormat sdf;

    private DatabaseReference mCommentsReference;
    private MainActivity.CommentAdapter mAdapter;



    static boolean calledAlready = false;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        sdf = new SimpleDateFormat("MMMM d, yyyy 'at' h:mm a");


        mCommentsReference = FirebaseDatabase.getInstance().getReference("eboxes").child("277-30500").child("0").child("received");

        mCommentsReference.keepSynced(true);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mManager = new LinearLayoutManager(this);
        mManager.setReverseLayout(false);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(mManager);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        SharedPreferences mPrefs=PreferenceManager.getDefaultSharedPreferences(this);

        String name = mPrefs.getString("name",null);
        String email=mPrefs.getString("email",null);
        String photoUrl=mPrefs.getString("photoUrl",null);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View navHeaderView = navigationView.inflateHeaderView(R.layout.nav_header_main);

        TextView textViewName = (TextView)navHeaderView.findViewById(R.id.textViewUserName);
        TextView textViewEmail = (TextView)navHeaderView.findViewById(R.id.textViewEmail);
        CircleImageView imageViewPhoto=(CircleImageView) navHeaderView.findViewById(R.id.imageView);

        Log.e(TAG,email);
        textViewEmail.setText(email);
        textViewName.setText(name);
        Picasso.with(this).load(photoUrl).placeholder(R.drawable.logo_56dp).fit().centerCrop().into(imageViewPhoto);


        navigationView.setNavigationItemSelectedListener(this);


        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("Filter by type");
        spinnerArray.add("type 2");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.custom_spinner_textview, spinnerArray);

        adapter.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        Spinner sItems = (Spinner) findViewById(R.id.spinner_filty_by_type);
        sItems.setAdapter(adapter);



    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }




    @Override
    public void onStart() {
        super.onStart();

        // Add value event listener to the post
        // [START post_value_event_listener]
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the U

                // [END_EXCLUDE]
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                // [START_EXCLUDE]

                // [END_EXCLUDE]
            }
        };
        // [END post_value_event_listener]

        // Keep copy of post listener so we can remove it when app stops

        // Listen for comments
        mAdapter = new MainActivity.CommentAdapter(this, mCommentsReference);
        mRecyclerView.setAdapter(mAdapter);
    }
    @Override
    public void onStop() {
        super.onStop();
        // Clean up comments listener
        mAdapter.cleanupListener();
    }

    //    TODO UNCOMMENT FOR LISTENERS TO WORK
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private static class CommentViewHolder extends RecyclerView.ViewHolder {

        public TextView message;
        public TextView TextViewTypeAlertBillSummon;
        public TextView textViewFileName;
        public TextView textViewPdf;
        public TextView textViewAddedTime;
        public ImageView imageViewSenderProfile;
        public TextView textViewDisplyName;

        public Button btnPayShare;
        public TextView textViewAmount;

        public TextView textViewCurrency;



        public CommentViewHolder(View itemView) {
            super(itemView);

            TextViewTypeAlertBillSummon=(TextView) itemView.findViewById(R.id.textViewTypeAlertBillSummon);
            textViewFileName=(TextView) itemView.findViewById(R.id.textViewFileName);
            textViewPdf=(TextView) itemView.findViewById(R.id.textViewPdf);

            btnPayShare=(Button) itemView.findViewById(R.id.btnPayShare);
            textViewAmount=(TextView) itemView.findViewById(R.id.textViewAmount);

            textViewAddedTime=(TextView) itemView.findViewById(R.id.textViewAddedTime);
            imageViewSenderProfile=(ImageView) itemView.findViewById(R.id.imageViewSenderProfile);
            textViewDisplyName=(TextView) itemView.findViewById(R.id.textViewDisplyName);
            message = (TextView) itemView.findViewById(R.id.textview1);

            textViewCurrency=(TextView) itemView.findViewById(R.id.textViewCurrency);
        }
    }

    private static class CommentAdapter extends RecyclerView.Adapter<MainActivity.CommentViewHolder> {

        private Context mContext;
        private DatabaseReference mDatabaseReference;
        private ChildEventListener mChildEventListener;

        private List<String> mCommentIds = new ArrayList<>();
        private List<ItemsReceived> itemsReceivedList = new ArrayList<>();

        public CommentAdapter(final Context context, DatabaseReference ref) {
            mContext = context;
            mDatabaseReference = ref;

            // Create child event listener
            // [START child_event_listener_recycler]
            ChildEventListener childEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                    Log.d(TAG, "onChildAdded:" + dataSnapshot.getKey());

                    // A new comment has been added, add it to the displayed list
                    ItemsReceived itemsReceived = dataSnapshot.getValue(ItemsReceived.class);
                    Toast.makeText(context,"A new message has arrived ",Toast.LENGTH_LONG);

                    // [START_EXCLUDE]
                    // Update RecyclerView
                    mCommentIds.add(dataSnapshot.getKey());
                    itemsReceivedList.add(itemsReceived);
                    notifyItemInserted(itemsReceivedList.size() - 1);
                    // [END_EXCLUDE]
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                    Log.d(TAG, "onChildChanged:" + dataSnapshot.getKey());

                    // A comment has changed, use the key to determine if we are displaying this
                    // comment and if so displayed the changed comment.
                    ItemsReceived newItemsReceived = dataSnapshot.getValue(ItemsReceived.class);
                    String commentKey = dataSnapshot.getKey();

                    // [START_EXCLUDE]
                    int commentIndex = mCommentIds.indexOf(commentKey);
                    if (commentIndex > -1) {
                        // Replace with the new data
                        itemsReceivedList.set(commentIndex, newItemsReceived);

                        // Update the RecyclerView
                        notifyItemChanged(commentIndex);
                    } else {
                        Log.w(TAG, "onChildChanged:unknown_child:" + commentKey);
                    }
                    // [END_EXCLUDE]
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    Log.d(TAG, "onChildRemoved:" + dataSnapshot.getKey());

                    // A comment has changed, use the key to determine if we are displaying this
                    // comment and if so remove it.
                    String commentKey = dataSnapshot.getKey();

                    // [START_EXCLUDE]
                    int commentIndex = mCommentIds.indexOf(commentKey);
                    if (commentIndex > -1) {
                        // Remove data from the list
                        mCommentIds.remove(commentIndex);
                        itemsReceivedList.remove(commentIndex);

                        // Update the RecyclerView
                        notifyItemRemoved(commentIndex);
                    } else {
                        Log.w(TAG, "onChildRemoved:unknown_child:" + commentKey);
                    }
                    // [END_EXCLUDE]
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                    Log.d(TAG, "onChildMoved:" + dataSnapshot.getKey());

                    // A comment has changed position, use the key to determine if we are
                    // displaying this comment and if so move it.
                    ItemsReceived movedItemsReceived = dataSnapshot.getValue(ItemsReceived.class);
                    String commentKey = dataSnapshot.getKey();

                    // ...
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.w(TAG, "postComments:onCancelled", databaseError.toException());
                    Toast.makeText(mContext, "Failed to load comments.",
                            Toast.LENGTH_SHORT).show();
                }
            };
            ref.addChildEventListener(childEventListener);
            // [END child_event_listener_recycler]


            // Store reference to listener so it can be removed on app stop
            mChildEventListener = childEventListener;
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(getItemCount() - 1 - position);

        }

        @Override
        public MainActivity.CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            View view = inflater.inflate(R.layout.item_recieved_layout, parent, false);
            return new MainActivity.CommentViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MainActivity.CommentViewHolder holder, int position) {
            ItemsReceived itemsReceived = itemsReceivedList.get(position);
            holder.message.setText(itemsReceived.getMessage());
            holder.TextViewTypeAlertBillSummon.setText(itemsReceived.getType());


            if (itemsReceived.getAttachments()!=null){
                holder.textViewFileName.setVisibility(View.VISIBLE);
                holder.textViewPdf.setVisibility(View.VISIBLE);
                holder.textViewFileName.setText(itemsReceived.getAttachments().get(0).getName());
            }
            if (itemsReceived.getType().equals("Bill")){
                holder.TextViewTypeAlertBillSummon.setTextColor(mContext.getResources().getColor(R.color.colorBill));
                holder.TextViewTypeAlertBillSummon.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.circle_bill),null,null,null);

                holder.btnPayShare.setText("PAY BILL");
                holder.textViewAmount.setText(itemsReceived.getData().getAmount());
                holder.textViewCurrency.setText("Ksh");

                holder.textViewCurrency.setVisibility(View.VISIBLE);
                holder.btnPayShare.setVisibility(View.VISIBLE);
                holder.textViewAmount.setVisibility(View.VISIBLE);
            }
            else if (itemsReceived.getType().equals("Alert")){

                holder.btnPayShare.setVisibility(View.VISIBLE);
                holder.btnPayShare.setText("SHARE ALERT");

                holder.TextViewTypeAlertBillSummon.setTextColor(mContext.getResources().getColor(R.color.colorRed));
                holder.TextViewTypeAlertBillSummon.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.circle_alert),null,null,null);

            }
            else {
                holder.TextViewTypeAlertBillSummon.setTextColor(mContext.getResources().getColor(R.color.colorSummon));
                holder.TextViewTypeAlertBillSummon.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.circle_summon),null,null,null);

            }

            holder.textViewAddedTime.setText(sdf.format(itemsReceived.getAdded()));

            Picasso.with(mContext).load(itemsReceived.getSenderProfile()).into(holder.imageViewSenderProfile);

            if (itemsReceived.getSender()!=null)
                holder.textViewDisplyName.setText(itemsReceived.getSender().getDisplayName());


        }

        @Override
        public int getItemCount() {
            return itemsReceivedList.size();
        }

        public void cleanupListener() {
            if (mChildEventListener != null) {
                mDatabaseReference.removeEventListener(mChildEventListener);
            }
        }

    }
}

