package com.siafuinc.eposta.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.siafuinc.eposta.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginRegisterSliderFragment extends Fragment {

    private static final String KEY_STRING = "string";
    private static final String KEY_IMAGE = "image";

    @BindView(R.id.image_view_background)
    ImageView imageViewBackground;
    @BindView(R.id.text_view_desc)
    TextView textViewDesc;

    Unbinder unbinder;

    public LoginRegisterSliderFragment() {
        // Required empty public constructor
    }

    public static LoginRegisterSliderFragment newInstance(String text, int image) {

        Bundle args = new Bundle();
        args.putString(KEY_STRING,text);
        args.putInt(KEY_IMAGE,image);
        LoginRegisterSliderFragment fragment = new LoginRegisterSliderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login_register_slider, container, false);
        unbinder = ButterKnife.bind(this,view);

        if(getArguments()!=null){
            String text = getArguments().getString(KEY_STRING);
            int image = getArguments().getInt(KEY_IMAGE);

            textViewDesc.setText(text);
            Picasso.with(getActivity()).load(image).fit().centerCrop().into(imageViewBackground);
        }
        return view;
    }


    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
