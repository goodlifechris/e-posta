package com.siafuinc.eposta.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.siafuinc.eposta.R;
import com.siafuinc.eposta.activities.DeliveryActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyPhysicalBox extends Fragment {


    public MyPhysicalBox() {
        // Required empty public constructor
    }
    public static Fragment newInstance() {
        Bundle args = new Bundle();
        MyPhysicalBox fragment = new MyPhysicalBox();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_my_physical_box, container, false);

        Button buttonDeliver=(Button) view.findViewById(R.id.btn_deliver);
        buttonDeliver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), DeliveryActivity.class));
            }
        });
        return view;
    }

}
