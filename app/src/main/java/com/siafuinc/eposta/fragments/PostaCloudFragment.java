package com.siafuinc.eposta.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.siafuinc.eposta.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostaCloudFragment extends Fragment {


    public PostaCloudFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_posta_cloud, container, false);
    }

}
