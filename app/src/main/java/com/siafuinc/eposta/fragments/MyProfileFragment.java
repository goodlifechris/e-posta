package com.siafuinc.eposta.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.siafuinc.eposta.R;
import com.siafuinc.eposta.activities.MoreProfileServicesActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfileFragment extends Fragment {


    @BindView(R.id.btn_more_profile_services)
    Button buttonGoToMoreServices;


    public MyProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_my_profile, container, false);
        Button btn=(Button) view.findViewById(R.id.btn_more_profile_services);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MoreProfileServicesActivity.class);
                startActivity(intent);
            }
        });
        ButterKnife.bind(view);

        return view;
    }

}
