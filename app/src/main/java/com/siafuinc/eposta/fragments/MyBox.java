package com.siafuinc.eposta.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.siafuinc.eposta.R;
import com.siafuinc.eposta.adapter.ReceivedItemsAdapter;
import com.siafuinc.eposta.model.ItemsReceived;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyBox extends Fragment {
    public static final String EXTRA_POST_KEY = "post_key";
    private static final String TAG ="myBox" ;

    private DatabaseReference mCommentsReference;
    private ValueEventListener mPostListener;
    private String mPostKey;
    ReceivedItemsAdapter mAdapter;
    private RecyclerView mRecyclerView;

    private LinearLayoutManager mManager;
    static SimpleDateFormat sdf;

    public Query query;

    public MyBox() {
        // Required empty public constructor
    }
    public static Fragment newInstance() {
        Bundle args = new Bundle();
        MyBox fragment = new MyBox();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_my_box, container, false);
        ButterKnife.bind(this,view);
        // Initialize Database
        mCommentsReference = FirebaseDatabase.getInstance().getReference("eboxes").child("277-30500").child("0").child("received");
//        query=mCommentsReference.orderByChild("added");
        mCommentsReference.keepSynced(true);

        // Initialize Views
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mManager = new LinearLayoutManager(getContext());
//        mManager.setReverseLayout(false);
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);

        mRecyclerView.setNestedScrollingEnabled(true);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(mManager);

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("Filter by type");
        spinnerArray.add("type 2");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getContext(), R.layout.custom_spinner_textview, spinnerArray);

        adapter.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        Spinner sItems = (Spinner) view.findViewById(R.id.spinner_filty_by_type);
        sItems.setAdapter(adapter);


        return view;
    }
    @Override
    public void onStart() {
        super.onStart();

        // Add value event listener to the post
        // [START post_value_event_listener]
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the U

                // [END_EXCLUDE]
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                // [START_EXCLUDE]

                // [END_EXCLUDE]
            }
        };
        // [END post_value_event_listener]

        // Keep copy of post listener so we can remove it when app stops

        // Listen for comments
        mAdapter = new ReceivedItemsAdapter(getContext(), mCommentsReference);
        mRecyclerView.setAdapter(mAdapter);
    }
    @Override
    public void onStop() {
        super.onStop();
        // Clean up comments listener
        mAdapter.cleanupListener();
    }



}
