package com.siafuinc.eposta.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.siafuinc.eposta.R;
import com.siafuinc.eposta.adapter.ViewPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link MainMyEBoxAndPhysicalAccount#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainMyEBoxAndPhysicalAccount extends Fragment {
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    ViewPagerAdapter viewPagerAdapter;
    PagerAdapter pagerAdapter;
    Unbinder unbinder;

    public MainMyEBoxAndPhysicalAccount() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static MainMyEBoxAndPhysicalAccount newInstance(String param1, String param2) {
        MainMyEBoxAndPhysicalAccount fragment = new MainMyEBoxAndPhysicalAccount();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_main_my_ebox_and_physical_account, container, false);
        unbinder = ButterKnife.bind(this,view);

        setupViewPager(viewPager);

        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        FragmentManager fragmentManager =  getActivity().getSupportFragmentManager();
        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFragment(MyBox.newInstance(), "E-LETTERS");
        viewPagerAdapter.addFragment(MyPhysicalBox.newInstance(), "P-LETTERS");

        viewPager.setAdapter(viewPagerAdapter);

        if(tabLayout!=null){
            tabLayout.setupWithViewPager(viewPager);
        }
    }




}
