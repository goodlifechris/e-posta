package com.siafuinc.eposta;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.siafuinc.eposta.activities.Main3Activity;
import com.siafuinc.eposta.activities.MainEboxActivity;
import com.siafuinc.eposta.activities.SignInDialogActivty;
import com.siafuinc.eposta.fragments.LoginRegisterSliderFragment;
import com.siafuinc.eposta.model.UserEboxFb;
import com.siafuinc.eposta.model.UserFb;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    GoogleApiClient mGoogleApiClient;
    GoogleSignInOptions gso;

    private static final int RC_SIGN_IN = 9001;
    private TextView mStatusTextView;
    private ProgressDialog mProgressDialog;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    private DatabaseReference mDatabase;


    // [START declare_auth_listener]
    private FirebaseAuth.AuthStateListener mAuthListener;
    // [END declare_auth_listener]

    public long eboxUserNo;


    private SectionsPagerAdapter mSectionsPagerAdapter;

    int dotCount;
    ImageView[] imageViewDots;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.layout_view_pager_count_dots)
    LinearLayout layoutCountDots;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set  up the ViewPager with the sections adapter.
        viewPager.setAdapter(mSectionsPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for(int i = 0; i < dotCount; i++){
                    imageViewDots[i].setImageResource(R.drawable.shape_non_selected_page);
                }
                imageViewDots[position].setImageResource(R.drawable.shape_selected_page);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                int current = viewPager.getCurrentItem();

                if(state == ViewPager.SCROLL_STATE_DRAGGING||state == ViewPager.SCROLL_STATE_SETTLING)
                    imageViewDots[current].setImageResource(R.drawable.shape_dragging_state);

                else if(state == ViewPager.SCROLL_STATE_IDLE) imageViewDots[current].setImageResource(R.drawable.shape_selected_page);

            }
        });

        dotCount = mSectionsPagerAdapter.getCount();
        imageViewDots = new ImageView[dotCount];

        for(int i = 0; i < dotCount; i++){
            imageViewDots[i] = new ImageView(this);
            imageViewDots[i].setImageResource(R.drawable.shape_non_selected_page);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            params.setMargins(8,0,8,0);

            layoutCountDots.addView(imageViewDots[i],params);

        }

        imageViewDots[0].setImageResource(R.drawable.shape_selected_page);

//        final ImageView imageViewHome=(ImageView) findViewById(R.id.imageView_home);
//        Picasso.with(this).load(R.drawable.lady).fit().centerCrop().into(imageViewHome);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);


        //Initializing google signin option
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();

        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //Initializing signinbutton
        final SignInButton signInButton=(SignInButton) findViewById(R.id.btn_get_pin);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setScopes(gso.getScopeArray());


        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]


        // [START auth_state_listener]
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    Intent i = new Intent(getBaseContext(), MainEboxActivity.class);
                    startActivity(i);

                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // [START_EXCLUDE]
                updateUIFb(user);
                // [END_EXCLUDE]
            }
        };
        mDatabase = FirebaseDatabase.getInstance().getReference();

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();

            }
        });

    }
    private void updateUIFb(FirebaseUser user) {
        hideProgressDialog();
        if (user != null) {
           Log.e(TAG,user.getUid()+" user id "+user.getEmail()+"");
            SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putString("name",user.getDisplayName());
            editor.putString("email",user.getEmail());
            if(user.getPhotoUrl()!=null) {
                editor.putString("photoUrl",String.valueOf(user.getPhotoUrl()));}
            editor.commit();

            //SO get the user first and check oif he exists
//            If Not the user has no accocunt

            //secondly may the buttons workfr


        } else {
            Log.e(TAG," user is signed out");

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onStop() {
        super.onStop();
//        mGoogleApiClient.connect();

        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

        mGoogleApiClient.connect();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }

    }

    // [START onActivityResult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (resultCode==RESULT_OK){
            if (requestCode == RC_SIGN_IN) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
            }
        }

    }
    // [END onActivityResult]

    // [START handleSignInResult]
    private void handleSignInResult(GoogleSignInResult result) {
//        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
//            mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            Log.e(TAG,"User is"+acct.getEmail());

            showAddNew(acct);

           //firebaseAuthWithGoogle(acct);


            //TODO CREATE DIALOG TO ASK FOR USER TO CREATE A NEW

//            check if user exists or not exist


            // Google Sign In was successful, authenticate with Firebase
//            firebaseAuthWithGoogle(acct);

            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }
    public void showAddNew(final GoogleSignInAccount acct){
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_set_up_user);
        // Set dialog title
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        dialog.show();

        // set values for custom dialog components - text, image and button

        TextView textViewusername = (TextView) dialog.findViewById(R.id.textViewUserName);
        TextView textViewEmail = (TextView) dialog.findViewById(R.id.textViewEmail);
        CircleImageView circleImageViewPhoto = (CircleImageView) dialog.findViewById(R.id.circleImageViewPhoto);
        final Spinner spinnerLocation=(Spinner) dialog.findViewById(R.id.spinner_get_location);
        final Spinner spinnerYesNo=(Spinner)dialog.findViewById(R.id.spinner_yes_no);
        spinnerLocation.setVisibility(View.GONE);

        final TextInputEditText editTextPostaCode=(TextInputEditText)dialog.findViewById(R.id.editpostalCode);
        final TextInputEditText editTextPhoneNumber=(TextInputEditText)dialog.findViewById(R.id.editTextPhoneNumber);

        final TextInputEditText editTextBoxNo=(TextInputEditText)dialog.findViewById(R.id.edit_Box_no);
        final RelativeLayout relativeLayout=(RelativeLayout) dialog.findViewById(R.id.relativelayout_location);
        final TextView textViewLocation=(TextView) dialog.findViewById(R.id.textViewLocation);


        final int min = 11000;
        final int max = 88000;
        final Random r = new Random();
        final int random = r.nextInt(max - min) + min;

        editTextPostaCode.setCursorVisible(false);
        editTextPostaCode.setFocusable(false);
        editTextPostaCode.setFocusableInTouchMode(false);

        editTextBoxNo.setCursorVisible(false);
        editTextBoxNo.setFocusable(false);
        editTextBoxNo.setFocusableInTouchMode(false);


        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("Select location");
        spinnerArray.add("Nairobi");
        spinnerArray.add("Kisumu");
        spinnerArray.add("Eldoret");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.custom_spinner_textview_sign_up, spinnerArray);
        adapter.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        spinnerLocation.setAdapter(adapter);

        List<String> spinnerYesNoArray =  new ArrayList<String>();
        spinnerYesNoArray.add("No");
        spinnerYesNoArray.add("Yes");

        ArrayAdapter<String> adapterYesNo = new ArrayAdapter<String>(
                this, R.layout.custom_spinner_textview_sign_up, spinnerYesNoArray);
        adapter.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        spinnerYesNo.setAdapter(adapterYesNo);

        spinnerYesNo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                editTextBoxNo.setText(null);
                editTextPostaCode.setText(null);

                if (i==0){
                    spinnerLocation.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.VISIBLE);
                    textViewLocation.setVisibility(View.VISIBLE);
                    editTextPostaCode.setCursorVisible(false);
                    editTextPostaCode.setFocusable(false);
                    editTextPostaCode.setFocusableInTouchMode(false);

                    editTextBoxNo.setCursorVisible(false);
                    editTextBoxNo.setFocusable(false);
                    editTextBoxNo.setFocusableInTouchMode(false);


                }
                else if (i==1){
                    spinnerLocation.setVisibility(View.GONE);
                    relativeLayout.setVisibility(View.GONE);
                    textViewLocation.setVisibility(View.GONE);
                    editTextPostaCode.setCursorVisible(true);
                    editTextPostaCode.setFocusable(true);
                    editTextPostaCode.setFocusableInTouchMode(true);

                    editTextBoxNo.setCursorVisible(true);
                    editTextBoxNo.setFocusable(true);
                    editTextBoxNo.setFocusableInTouchMode(true);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


//        final int random = Random.nextInt((max - min) + 1) + min;

        spinnerLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i==1){
                    editTextPostaCode.setText("00200");
                    editTextBoxNo.setText(String.valueOf(r.nextInt(max - min) + min));

                }
                else if (i==2){
                    editTextPostaCode.setText("00300");
                    editTextBoxNo.setText(String.valueOf(r.nextInt(max - min) + min));

                }
                else if (i==3){
                    editTextPostaCode.setText("00400");
                    editTextBoxNo.setText(String.valueOf(r.nextInt(max - min) + min));

                }
                else{}

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Picasso.with(this).load(acct.getPhotoUrl()).into(circleImageViewPhoto);
        textViewusername.setText(acct.getDisplayName());
        textViewEmail.setText(acct.getEmail());

       // dialog.show();

        Button declineButton = (Button) dialog.findViewById(R.id.btn_save);
        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                if (editTextPhoneNumber.getText().toString().trim().length()== 0){
                    editTextPhoneNumber.setError("Please fill phone number");
                }

                else if (editTextBoxNo.getText().toString().trim().length()== 0){
                    editTextBoxNo.setError("Please fill Box Number");
                }
                else if (editTextPostaCode.getText().toString().trim().length()== 0){
                    editTextPostaCode.setError("Please fill Postal Code Number");
                }


                else {
                    dialog.dismiss();
                    firebaseAuthWithGoogle(acct,String.valueOf(editTextBoxNo.getText()),String.valueOf(editTextPostaCode.getText()));

                }

            }
        });

    }


    // [START auth_with_google]
    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct,final String POBox,final String postalCode) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        // [START_EXCLUDE silent]
        showProgressDialog();
        // [END_EXCLUDE]

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        // Write a message to the database
                        //To Trim all the nonsense if possible
//                        String POBox="00101 ";
//                        String postalCode="105458 ";
                        final String eboxId=(postalCode.trim()+"-"+POBox.trim());

                        Log.e(TAG,"Ebox ID is "+eboxId);
//TODO CHECK IF USER EXISTS IN THE main AUTH DATABASE
                        mDatabase.child("users").child(acct.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshot) {
                                if (snapshot.exists()) {
                                    // TODO: handle the case where the data already exists
                                    Log.e(TAG,"User exits");


                                    Intent i = new Intent(getBaseContext(), MainEboxActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                                else {
                                    Log.e(TAG,"User doesnt exits");
//TODO CHECK IF USER EXISTS IN THE USER  DATABASE
                                    checkifEboxForUserExists(eboxId,mDatabase,acct.getId(),POBox.trim(),acct.getEmail(),acct.getDisplayName(),postalCode.trim());
//                                    saveNewUser(acct.getId(),"55280",String.valueOf(checkifEboxForUserExists(eboxId,mDatabase)),acct.getEmail(),acct.getDisplayName(),"00200");
                                    //TODO ONCE SAVED TO THE USER WE HAVE TO SAVE TO THE EBOX TOO
                                    // TODO: handle the case where the data does not yet exist
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }

                        });
                        // [START_EXCLUDE]
                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END auth_with_google]
    // [END handleSignInResult]

    // [START signIn]
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }
//    private void getMyReceivedItems(DatabaseReference mDatabase,String eboXNumber,Long myEboxIndex){
////        myRef.orderByChild("ci").equalTo("6991580").addListenerForSingleValueEvent(...
////        Query queryRef = myFirebaseRef.orderByChild("fullName").equalTo("gooner");
////        Query query = ref.orderByChild("Author").equalTo("Author1", "Author");
////        query.addChildEventListener(new ChildEventListener() {
////
////                                    }
//        mDatabase.child("eboxes").child(eboXNumber).child("myEboxIndex").addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//        mDatabase.child("eboxes").child(eboXNumber).child("myEboxIndex").addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot snapshot) {
//                if (snapshot.getChildrenCount() != 0) {
//                    Iterable<DataSnapshot> i = snapshot.getChildren();
//                    for (DataSnapshot postSnapshot: snapshot.getChildren()) {
//                        // Find the particular node (e.g. last node, first node... etc) here and create a object from it.
//                        UserEboxFb foundPost = postSnapshot.getValue(UserEboxFb.class);
//
//                    }
//            }
//
//
//
//        }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//
//
//    });
//    }

    private void  checkifEboxForUserExists(String EboxNumber, DatabaseReference database, final String UserId, final String POB, final String email, final String name, final String postalCode){


        database.child("eboxes").child(EboxNumber).addListenerForSingleValueEvent(new ValueEventListener() {


            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    // TODO: handle the case where the data already exists
                    Log.e(TAG,"Ebox exists with the length of"+snapshot.getChildrenCount());
//We add here
                    saveNewUser(UserId,POB,""+snapshot.getChildrenCount(),email,name,postalCode);

                    Intent i = new Intent(getBaseContext(), MainEboxActivity.class);
                    startActivity(i);

                }
                else {
                    Log.e(TAG,"Ebox is new ");
                    saveNewUser(UserId,POB,"0",email,name,postalCode);
                    Intent i = new Intent(getBaseContext(), MainEboxActivity.class);
                    startActivity(i);
                    finish();
//                    saveNewUser(acct.getId(),"55280","0",acct.getEmail(),acct.getDisplayName(),"00200");
                    // TODO: handle the case where the data does not yet exist
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        Log.e(TAG,"Ebox No is "+ eboxUserNo);

    }


    // [END signIn]

    private void saveNewUser(String UserId,String POB, String eBoxIndex, String email, String name, String postalCode) {
        UserFb user = new UserFb(POB,eBoxIndex,email,name,postalCode);
        mDatabase.child("users").child(UserId).setValue(user);
    }
    private void saveNewUserTOEbox(String UserId,String POB, String eBoxIndex, String email, String name, String postalCode) {
        UserFb user = new UserFb(POB,eBoxIndex,email,name,postalCode);
        mDatabase.child("users").child(UserId).setValue(user);
    }


                                                                               // [START signOut]
    public void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END signOut]

    // [START revokeAccess]
    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END revokeAccess]

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("loading");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    private void updateUI(boolean signedIn) {
        if (signedIn) {
            Log.e(TAG,"User is logged in");
        } else {
            Log.e(TAG,"User is signed out");
//            mStatusTextView.setText(R.string.signed_out);
//            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
//            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.GONE);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }



    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return LoginRegisterSliderFragment.newInstance("Convenient", R.drawable.lady);
                case 1:
                    return LoginRegisterSliderFragment.newInstance("Consistent", R.drawable.posta_box);
                case 2:
                    return LoginRegisterSliderFragment.newInstance("Reliable", R.drawable.posta_children);

            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

    }
}
